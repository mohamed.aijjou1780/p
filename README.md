![](logo.png)

# NutriCheckFood 🥫

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.2.

Cette application va permettre à chacun d'obtenir des informations sur la qualité nutritionnelle des produits qu'ils consomment régutierement.
A travers son scanner ou son champ de saisie, les EAN des produits vont fourniront des données tels que le nutri-score, la liste des ingrédients ou l'epreinte carbone d'un produit.
```diff
-  🚨 Si le projet est ouvert dans un navigateur PC, il est important d'ouvrir ce dernier en mode console et de choisir une visualisation en mode "Mobile", de preference Iphone 6/7/8 par exemple 🚨.
```

## Description
![forthebadge](https://forthebadge.com/images/badges/made-with-typescript.svg) ![forthebadge](https://forthebadge.com/images/badges/built-by-developers.svg) ![forthebadge](https://forthebadge.com/images/badges/powered-by-responsibility.svg)

![Video-demo](video-nutri-check.mov)


```diff
-  🚨 Si le projet est ouvert dans un navigateur PC, il est important d'ouvrir ce dernier en mode console et de choisir une visualisation en mode "Mobile", de preference Iphone 6/7/8 par exemple 🚨.
```

## Get Starting

### Options

2 possibiltés pour découvrir cette application :

* 1 -  En installant et demarrant le projet en local.
* 2 -  Directement en se connectant à l'adresse suivante : https://nutricheckfood.firebaseapp.com.


### Build && Dependances

* Angular.
* Bootstrap.
* mg-Bootstrap.
* Material-Angular.
* Ngx-barcode-scanner.
* Firebase.
* Api : https://fr.openfoodfacts.org

### Installation

* Etape 1 : ``` git clone https://gitlab.com/mohamed.aijjou1780/p.git ```
* Etape 2 : ``` npm install -g @angular/cli ```
* Etape 3 : ``` npm install ``` 
* Etape 4 : ``` npm start ou ng serve ```


### Authors

2020 - now : **Mohamed Aijjou**

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
