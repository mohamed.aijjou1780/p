import { AccueilComponent } from './Page/Accueil/accueil.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreationComponent } from './Page/Creation/creation.component';
import { ValidationComponent } from './Page/Validation/validation.component';
import { ConnexionComponent } from './Page/Connexion/connexion.component';
import { AuthGuardService } from './services/auth-guard.service';
import { ScannerComponent } from './Page/Scanner/scanner.component';
import { SaisieComponent } from './Page/Saisie/saisie.component';
import { ResultatComponent } from './Page/Resultat/resultat.component';
import { HomeComponent } from './Page/Home/home.component';


const routes: Routes = [
  {path: 'creation', component: CreationComponent},
  {path: 'validation', component: ValidationComponent},
  {path: 'home', component:  HomeComponent},
  {path: 'accueil', component: AccueilComponent},
  {path: 'connexion', component: ConnexionComponent},
  {path: 'scanner', canActivate: [AuthGuardService], component: ScannerComponent},
  {path: 'saisie', canActivate: [AuthGuardService], component: SaisieComponent},
  {path: 'resultat/:code', canActivate: [AuthGuardService],component: ResultatComponent},
  {path: '',redirectTo: 'home', pathMatch: 'full'},
  {path: '**',redirectTo: 'home'},
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
