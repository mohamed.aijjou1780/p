
export interface Product {
  code: number;
  product: product;



}
export interface product {
  product_name_fr : string;
  brands_tags: string;
  quantity : string;
  allergens_from_ingredients: string;
  labels: string;
  nutriments :Nutriments;
  ingredients_text : string;
  categories: string;
  carbon_footprint_percent_of_known_ingredients : string;
  ingredients_text_fr: string;
  image_nutrition_url: string;
  nutriscore_grade : string;
  image_small_url: string;
  nutrient_levels : Nutrient_levels;
  ingredients_analysis_tags: string[];
  ingredients_from_palm_oil_n;
 

}

export interface Nutrient_levels{
  sugars,
  fat,
  salt,
}

export interface Nutriments{
  salt_value,
  sugars_value,
  fat_value,
  sugars_unit,
  fat_unit,
  salt_unit,
  energy_unit : string;
  energy_value : number;
}