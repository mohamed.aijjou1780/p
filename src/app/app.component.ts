import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor() {
  const firebaseConfig = {
    apiKey: "AIzaSyAATO1ViyLDJEEMaCsqbTi3YY13B5hs0AE",
    authDomain: "projectopenfood1.firebaseapp.com",
    databaseURL: "https://projectopenfood1.firebaseio.com",
    projectId: "projectopenfood1",
    storageBucket: "",
    messagingSenderId: "728256238780",
    appId: "1:728256238780:web:71222876b4910ac3"
  };


  firebase.initializeApp(firebaseConfig);
  }
}
