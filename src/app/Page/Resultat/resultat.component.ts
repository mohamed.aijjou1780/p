import { ProductService } from "../../services/product.service";
import { Component, OnInit, Input } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Product } from "src/app/models/product_model";
import { element } from "protractor";
import { stringify } from "@angular/compiler/src/util";

@Component({
  selector: "app-resultat",
  templateUrl: "./resultat.component.html",
  styleUrls: ["./resultat.component.scss"]
})
export class ResultatComponent implements OnInit {
  public code: number;
  public product: Product;
  public nutriSalt: string;
  public nombre: number;
  public salt: string;
  public fat: string;
  public sugar: string;
  public vegan: string[];
  public veganInfos: string;
  public palmInfos: number;
  public valSalt : number;
  public valFat : number;
  public valSugar : number;
  public fatUnit : string;
  public sugarUnit : string;
  public saltUnit : string;
  public energiUnit : string;
  public energiValue : number;
  public quantite : string;

  constructor(
    private route: ActivatedRoute,
    private prodductService: ProductService
  ) {}

  ngOnInit() {
    this.code = this.route.snapshot.params.code;

    this.prodductService.getProducts(this.code).subscribe((res: Product) => {
      this.product = res;

      this.salt = this.product.product.nutrient_levels.salt;
      this.fat = this.product.product.nutrient_levels.fat;
      this.sugar = this.product.product.nutrient_levels.sugars;
      this.vegan = this.product.product.ingredients_analysis_tags;
      this.palmInfos = this.product.product.ingredients_from_palm_oil_n;
      this.valSalt = this.product.product.nutriments.salt_value;
      this.valFat = this.product.product.nutriments.fat_value;
      this.valSugar = this.product.product.nutriments.sugars_value;
      this.sugarUnit = this.product.product.nutriments.sugars_unit;
      this.fatUnit = this.product.product.nutriments.fat_unit;
      this.saltUnit = this.product.product.nutriments.salt_unit;
this.energiUnit = this.product.product.nutriments.energy_unit;
this.energiValue = this.product.product.nutriments.energy_value;
this.quantite = this.product.product.quantity;


      
      console.log(this.vegan);
      this.CheckVeganAndPalm();
    });
  }

  CheckValSalt() {
    switch (this.salt) {
      case "moderate":
        return "orange";
      case "high":
        return "red";
      case "low":
        return "green";
    }
  }

  CheckValFat() {
    switch (this.fat) {
      case "moderate":
        return "orange";
      case "high":
        return "red";
      case "low":
        return "green";
    }
  }
  CheckValSugar() {
    switch (this.sugar) {
      case "moderate":
        return "orange";
      case "high":
        return "red";
      case "low":
        return "green";
    }
  }

  CheckVeganAndPalm(): string {
    this.vegan.forEach(element => {
      if (
        element.toString() == "en:vegan" ||
        element.toString() == "en:maybe-vegan"
      ) {
        console.log(element);
        this.veganInfos = "vegan";
      } else if (element.toString() == "en:non-vegan") {
        this.veganInfos = "non vegan";
      } else if (element.toString() == null) {
        this.veganInfos = "non concerne";
      }
    });
    return;
  }
}
