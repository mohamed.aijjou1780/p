import { ProductService } from "../../services/product.service";
import { Product } from "../../models/product_model";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Subject, Observable } from "rxjs";
import { Router } from "@angular/router";
import { AuthService } from "src/app/services/auth.service";
import * as firebase from "firebase";

@Component({
  selector: "app-saisie",
  templateUrl: "./saisie.component.html",
  styleUrls: ["./saisie.component.scss"]
})
export class SaisieComponent implements OnInit {
  public code: number;
  public product: Product;
  envoi: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.envoi = this.formBuilder.group({
      code: ["", [Validators.required]]
    });
  }

  onSubmit() {
    this.code = this.envoi.get("code").value;
    this.router.navigate(["resultat", this.code]);
  }

  onSignOut() {
    this.authService.signOutUser();
  }
}
