

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatCardModule} from '@angular/material/card';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreationComponent } from './Page/Creation/creation.component';
import { ValidationComponent } from './Page/Validation/validation.component';
import {MatIconModule} from '@angular/material/icon';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { AccueilComponent } from './Page/Accueil/accueil.component';
import {MatButtonModule} from '@angular/material/button';
import { ConnexionComponent } from './Page/Connexion/connexion.component';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from './services/auth.service';
import { AuthGuardService } from './services/auth-guard.service';
import { RouterModule } from '@angular/router';
import { ScannerComponent } from './Page/Scanner/scanner.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { AppUiModule } from './app-ui.module';
import { CommonModule } from '@angular/common';
import { SaisieComponent } from './Page/Saisie/saisie.component';
import { ResultatComponent } from './Page/Resultat/resultat.component';
import { HomeComponent } from './Page/Home/home.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    CreationComponent,
    ValidationComponent,
    HeaderComponent,
    AccueilComponent,
    ConnexionComponent,
    ScannerComponent,
    SaisieComponent,
    ResultatComponent,
    HomeComponent,

  ],
  imports: [
    NgbModule,
    MatCardModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonToggleModule,
    ReactiveFormsModule,
    MatIconModule,
    FormsModule,
    MatButtonModule,
    ReactiveFormsModule,
    HttpClientModule,
    CommonModule,
    AppUiModule,
    RouterModule,
    MatSnackBarModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),

  ],
  providers: [AuthService, AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
