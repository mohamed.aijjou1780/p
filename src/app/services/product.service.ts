import { SaisieComponent } from '../Page/Saisie/saisie.component';
import { Product } from './../models/product_model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { MessageService } from '../message.service';
import { tap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private httpClient: HttpClient, private http: HttpClient,  private messageService: MessageService) { }

  private productUrl = 'https://world.openfoodfacts.org/api/v0/product';
  private term : number;



  public getProduct(): Observable<Product[]> {
    return this.httpClient.get<Product[]>('https://world.openfoodfacts.org/api/v0/product');
  }

  public getProducts(code: number): Observable<Product> {
    return this.httpClient.get<Product>('https://world.openfoodfacts.org/api/v0/product/' + code);
  }



}
